from flask import Flask, render_template
from flask_socketio import SocketIO, join_room, emit
from codenames import game

#initialize flask
app = Flask(__name__)
#wrap flask app withing SocketIO
socketio = SocketIO(app)
ROOMS = {} #dict to track active rooms

@app.route('/')
def index():
    """serve the index HTML"""
    return render_template('index.html')

@socketio.on('create')
def on_create(data):
    """Create a game lobby"""
    gm = game.Info(
        size=data['size'],
        teams=data['teams'],
        dictionary=data['dictionary'])
    room = gm.game_id
    ROOMS[room] = gm
    join_room(room)
    emit('join_room', {'room': room})

@socketio.on('join')
def on_join(data):
    """Join a game lobby"""
    # username = data['username']
    room = data['room']
    if room in ROOMS:
        # add player and rebroadcast game object
        # rooms[room].add_player(username)
        join_room(room)
        send(ROOMS[room].to_json(), room=room)
    else:
        emit('error', {'error': 'Unable to join room. Room does not exist.'})

@socketio.on('flip_card')
def on_flip_card(data):
    """flip card and rebroadcast game object"""
    room = data['room']
    card = data['card']
    ROOMS[room].flip_card(card)
    send(ROOMS[room].to_json(), room=room)

if __name__ == '__main__':
    socketio.run(app, debug=True)


